Make targets:

help:      this help
dl:        downloads source rpms
tbz2:      compress them into bz2
upload:    uploads it
clean:     cleans everything, except downloaded files
distclean: cleans up everything
