New development: actually, bind-dyndb-ldap works well until bind9.18, although it needs some
unofficial patch available only in fedora. There is no working bind-dyndb-ldap in debian
since buster.

Current idea is to use the fedora bind-dyndb-ldap with its bind9.18 as a master + admin
system, and to give all system a slave zone + caching bind to it. That renders the
anyways very good powerdns admin unusable, hope is that an equal quality bind9 zone
admin tool can be found.

Another problem is that fedora does not support i386 any more. However, as a master for slave
zones, performance is no issue, so we prefer the crystallic solutions - this time, an
extracted s390x package in /opt/bind, executed by a qemu-user-s390x.

---

Problem was that bind9 ldap backend does not work since debian buster. It deeply depends
on the bind9 internals which have changed a lot, beside that even they are not in the best
mood.

Decision is that we use powerdns with ldap backend to serve the ldap zone, and bind9 will
cache to it. At least, first. Later might we give more chance to powerdns or some
alternative. Although bind9 is good, we wait him back.
