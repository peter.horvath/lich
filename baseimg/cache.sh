#!/usr/bin/env -S bash
die() {
  echo "$*" >&2
  exit 1
}

if [ "$1" = "save" ]
then
  mkdir -pv cache/{var_cache_apt_archives,var_lib_apt}
  if [ -d root/var/cache/apt/archives ]
  then
    rsync -vaH root/var/cache/apt/archives/ cache/var_cache_apt_archives/
  fi
  if [ -d root/var/lib/apt/archives ]
  then
    rsync -vaH root/var/lib/apt/lists/ cache/var_lib_apt_lists/
  fi
elif [ "$1" = "load" ]
then
  mkdir -pv root/{var/cache/apt/archives,var/lib/apt/lists}
  if [ -d cache/var_cache_apt_archives ]
  then
    rsync -vaH cache/var_cache_apt_archives/ root/var/cache/apt/archives/
  fi
  if [ -d cache/var_lib_apt_lists ]
  then
    rsync -vaH cache/var_lib_apt_lists/ root/var/lib/apt/lists/
  fi
else
  die "usage: $0 load|save"
fi
