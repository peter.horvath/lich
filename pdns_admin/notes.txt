Problem was that bind9 ldap backend does not work since debian buster. It deeply depends
on the bind9 internals which have changed a lot, beside that even they are not in the best
mood.

Decision is that we use powerdns with ldap backend to serve the ldap zone, and bind9 will
cache to it. At least, first. Later might we give more chance to powerdns or some
alternative. Although bind9 is good, we wait him back.

Another two problems happened:

1. PowerDNS does not support 32 bit any more on braindamaged reasons ("32 bit time_t", fake).
Possible solutions: a) qemu-user with any 64-bit arch, that will be slow but it will work,
and slowness can be solved by caching bind frontends. b) there is a trivial patch to
re-enable 32 bit support and it will work. Although the patch is done in the "staging",
decision currently is to do (a).

2. Also PowerDNS bind does not support writing into LDAP, only read. It could be done easily
to write, but it would still need work. Solution: simple postgresql-backend pdns managed
by a simple, psql-backend pdns_admin (side problem: other pdns frontends need mysql...).
Later, on need, there are various ways to use data synchronization between psql, ldap,
bind and pdns in any direction. No such complexity is needed at the moment.)

---

Finally, the app can be started by the command

FLASK_APP=$PWD/powerdnsadmin/__init__.py FLASK_CONF=$PWD/powerdnsadmin/config.py gunicorn 'powerdnsadmin:create_app()' -t 120 --workers 4 --bind 0.0.0.0:9161 --log-level info

...only after the pyenv activated.
