/*
variable "default_incus_backend" {
  type = string
  default = "hwen"
}
*/

provider "incus" {
  remote {
    name = "hwen"
    scheme = "https"
    address = "192.168.220.33"
    // default = var.default_incus_backend == "hwen"
  }

  /*
  remote {
    name = "hpmaxx"
    scheme = "https"
    address = "192.168.220.36"
    // default = var.default_incus_backend == "hpmaxx"
  }
  */

  remote {
    name = "local"
    scheme = "unix"
    // default = var.default_incus_backend == "local"
  }
}

resource "incus_network" "incus0" {
  name = "incus0"
  type = "bridge"
  remote = "hwen"
  config = {
    // "parent" = "incus0"
    // "bridge.external_interfaces" = 
    // "bridge.hwaddr" =
    "dns.mode" = "none"
    "ipv4.address" = "192.168.220.97/27"
    "ipv4.dhcp" = false
    "ipv4.firewall" = false
    "ipv4.nat" = false
    "ipv4.routing" = true
    "ipv6.address" = "none"
  }
}

resource "incus_project" "default" {
  name = "default"
  remote = "hwen"
  config = {
    "features.networks" = true
    "features.networks.zones" = true
  }
  description = "default project"
}

resource "incus_profile" "default" {
  name = "default"
  remote = "hwen"
  description = "Default Incus profile"
}

resource "incus_profile" "mine" {
  name = "mine"
  remote = "hwen"
  description = "My Incus Profile"
  device {
    name = "root"
    type = "disk"
    properties = {
      path = "/"
      pool = "default"
    }
  }
  device {
    name = "eth0"
    type = "nic"
    properties = {
      "network" = "incus0"
    }
  }
}

resource "incus_storage_pool" "default" {
  name = "default"
  remote = "hwen"
  driver = "dir"
}

/*
resource "incus_volume" "first_root" {
  name   = "first_root"
  remote = "hwen"
  pool   = "default"
  type   = "custom"
  content_type = "filesystem"
  project = "default"
}*/

resource "incus_instance" "store" {
  name             = "store"
  remote           = "hwen"
  project          = "default"
  config           = {}
  image            = "trixie-i386"
  ephemeral        = false
  limits           = {}
  profiles         = [ "mine" ]
  running          = true
  type             = "container"
  wait_for_network = false
  device {
    name = "blob_rw"
    type = "disk"
    properties = {
      source = "/blob"
      path = "/blob"
      // readonly = true
      readonly = false
    }
  }
}

resource "incus_instance" "redmine" {
  name             = "redmine"
  remote           = "hwen"
  project          = "default"
  config           = {}
  image            = "trixie-i386"
  ephemeral        = false
  limits           = {}
  profiles         = [ "default" ]
  running          = true
  type             = "container"
  wait_for_network = false
  device {
    name = "root"
    type = "disk"
    properties = {
      path = "/"
      pool = "default"
    }
  }
  device {
    name = "blob"
    type = "disk"
    properties = {
      source = "/blob"
      path = "/blob"
      readonly = true
    }
  }
  device {
    name = "eth0"
    type = "nic"
    properties = {
      "network" = "incus0"
    }
  }
}

resource "incus_instance" "spielplatz" {
  name             = "spielplatz"
  remote           = "hwen"
  project          = "default"
  config           = {}
  image            = "trixie-i386"
  ephemeral        = false
  limits           = {}
  profiles         = [ "default" ]
  running          = true
  type             = "container"
  wait_for_network = false
  device {
    name = "root"
    type = "disk"
    properties = {
      path = "/"
      pool = "default"
    }
  }
  device {
    name = "blob"
    type = "disk"
    properties = {
      source = "/blob"
      path = "/blob"
      readonly = true
    }
  }
  device {
    name = "eth0"
    type = "nic"
    properties = {
      "network" = "incus0"
    }
  }
}

resource "incus_instance" "frontend" {
  name             = "frontend"
  remote           = "hwen"
  project          = "default"
  config           = {}
  image            = "trixie-i386"
  ephemeral        = false
  limits           = {}
  profiles         = [ "mine" ]
  running          = true
  type             = "container"
  wait_for_network = false
}

resource "incus_instance" "backend" {
  name             = "backend"
  remote           = "hwen"
  project          = "default"
  config           = {}
  image            = "trixie-i386"
  ephemeral        = false
  limits           = {}
  profiles         = [ "mine" ]
  running          = true
  type             = "container"
  wait_for_network = false
}

resource "incus_instance" "desktop" {
  name             = "desktop"
  remote           = "hwen"
  project          = "default"
  config           = {}
  image            = "trixie-i386"
  ephemeral        = false
  limits           = {}
  profiles         = [ "mine" ]
  running          = true
  type             = "container"
  wait_for_network = false
}

resource "incus_instance" "secu" {
  name             = "secu"
  remote           = "hwen"
  project          = "default"
  config           = {}
  image            = "trixie-i386"
  ephemeral        = false
  limits           = {}
  profiles         = [ "mine" ]
  running          = true
  type             = "container"
  wait_for_network = false
}

/*
resource "incus_instance" "t64" {
  name             = "t64"
  remote = "hwen"
  project          = "default"
  config           = {}
  image            = "f2ab91f79322"
  ephemeral        = false
  limits           = {}
  profiles         = [ "default" ]
  running          = true
  type             = "container"
  wait_for_network = false
  device {
    name = "root"
    type = "disk"
    properties = {
      path = "/"
      pool = "default"
    }
  }
  device {
    name = "blob"
    type = "disk"
    properties = {
      source = "/blob"
      path = "/blob"
      readonly = true
    }
  }
  device {
    name = "eth0"
    type = "nic"
    properties = {
      "network" = "incus0"
    }
  }
}
*/
