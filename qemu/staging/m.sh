#!/usr/bin/env zsh
# script to patch a debian package. Does not work - also various md5, sha sums has to be updated on the cd image,
# I do not want to develop the fix for all of them
set -e
set -x

tmp="$(mktemp -d -p /tmp)"

ar p work/preseed/pool/main/a/apt-setup/apt-cdrom-setup_0.182_all.udeb data.tar.xz |\
xz -d - >"$tmp/a.tar"

ar p work/preseed/pool/main/a/apt-setup/apt-mirror-setup_0.182_all.udeb data.tar.xz |\
xz -d - >"$tmp/b.tar"

tar --concatenate --file="$tmp/a.tar" "$tmp/b.tar"

xz -9vek "$tmp/a.tar"

cp -vfa "$tmp/a.tar.xz" "$tmp/data.tar.xz"

ar r work/preseed/pool/main/a/apt-setup/apt-mirror-setup_0.182_all.udeb "$tmp/data.tar.xz"
ar s work/preseed/pool/main/a/apt-setup/apt-mirror-setup_0.182_all.udeb

rm -rvf "$tmp"
