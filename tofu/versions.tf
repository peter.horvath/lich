terraform {
  backend "local" {
    path = "tofu.tfstate"
  }
  required_version = "1.8.1"
  required_providers {
    incus = {
      source = "lxc/incus"
      // version = "2.0.0"
    }
  }
}
