---
- name: Restart Lxd
  ansible.builtin.service:
    name: lxd
    state: restarted
