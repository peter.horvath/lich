What we have:

1. Large external stores. We deal with them case by case, independently.

2. Small(er) but important local stores.
  * They are persistent states or various internal services. Most commonly, database dumps.
  * We have a DAV based store for them. It can be apache dav or minio.
  * Decision is minio, due to the versioning and s3 compat. Although apache would be a better idea.
  * Software build scripts are generating an initial state from them, a store
      them in the minio.
  * Ansible package installs use the minio to download them.
  * Minio runs on the "store" VM, but it is backed up in the host (like /blob).
  * Normally, only a single service instance / VM is supported.
  * Typical service movement steps to another VM:
    # Dump state into minio.
    # Modify minio to promote dump to initial state of the service on the new place.
    # Deploy service into target by ansible, using the now dumped initial state.
    # Modify connected services to point to the new position.

---

An hour reading made me to decide for git-annex with an apache dav backend.
