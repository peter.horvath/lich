#!/bin/sh
. /.config

echo "auto ens4
allow-hotplug ens4
iface ens4 inet dhcp" >/target/etc/network/interfaces.d/ens4

if [ "$CONFIG_ROOT_LOGIN_BY_PASSWORD" = y ]
then
	sed -i 's/^.*PermitRootLogin.*$/PermitRootLogin yes/g' /etc/ssh/sshd_config
fi

if [ "$CONFIG_ROOT_LOGIN_BY_KEYS" = y ]
then
	mkdir -pv /target/root/.ssh
	echo "$CONFIG_SSH_AUTHORIZED_KEY" >target/root/.ssh/authorized_keys
	chmod -R go-rwx /target/root/.ssh
	chmod go-rwx /target/root
fi
