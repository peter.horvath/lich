#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: iputils

short_description: Ansible module for the Linux iputils package
version_added: "1.0.0"
description: Define linux iputils2 entities for ansible. At the time,
  we support only ip(4/6) address additions or removals.

options:
    addr:
        description: This is the address we want to have or we do not want to have.
        required: true
        type: str
    iface:
        description: This is the interface on which we want or do not want to have it.
        required: true
        type: str
    state:
        description: present if we want it (default) or absent if we do not want to have it.
        required: false
        type: str

author:
    - Peter Horvath (@peter.horvath)
'''

EXAMPLES = r'''
# Add an address
- name: Add an address to eth0
  iputils:
    addr: 192.168.220.1/24
    iface: eth0
    state: present

# Remove an address
- name: Remove an address from ens6
  iputils:
    addr: ::1/128
    iface: ens0
    state: absent
'''

RETURN = r'''
# Here is the JSON what the task execution shows at the end. Maybe we will have here also things.
# Now, only changed: true|false must be set up.
#
# These are examples of possible return values, and in general should use other names for return values.
original_message:
    description: The original name param that was passed in.
    type: str
    returned: always
    sample: 'hello world'
message:
    description: The output message that the test module generates.
    type: str
    returned: always
    sample: 'goodbye'
'''

from ansible.module_utils.basic import AnsibleModule

def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        addr=dict(type='str', required=True),
        iface=dict(type='str', required=True),
        state=dict(type='str', required=False, default='present')
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    if module.params not in ['present', 'absent']:
        module.fail_json(msg='invalid state, it must be present or absent', **result)

    cmd = subprocess.run(['ip', 'addr', 'ls', 'dev', module.params['iface']], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    if not isinstance(cmd.returncode, int) or cmd.returncode != 0:
        module.fail_json(msg='can not check existing ip addresses', **result)

    # if not re.search("(inet|inet6)
    # no. We just disable ipv6 everywhere and this module goes into staging (now)

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)

    cmd = subprocess.run(['ip', 'addr', 'dev', module.params['iface'], module.params['addr']])

    # manipulate or modify the state as needed (this is going to be the
    # part where your module will do what it needs to do)
    result['original_message'] = module.params['addr']
    result['message'] = 'goodbye'

    # use whatever logic you need to determine whether or not this module
    # made any modifications to your target
    if module.params['new']:
        result['changed'] = True

    # during the execution of the module, if there is an exception or a
    # conditional state that effectively causes a failure, run
    # AnsibleModule.fail_json() to pass in the message and the result
    if module.params['name'] == 'fail me':
        module.fail_json(msg='You requested this to fail', **result)

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
