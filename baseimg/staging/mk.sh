#!/usr/bin/env -S bash
set -x
set -e
mkdir -pv root
fakeroot -s "$PWD/state" fakechroot \
  debootstrap --variant=minbase --include=systemd --verbose \
    --cache-dir="$PWD/cache" --merged-usr \
    --arch=i386 trixie "$PWD/root" http://ftp.de.debian.org/debian/
fakeroot -i "$PWD/state" -s "$PWD/state" \
  tar -C "$PWD/root" -cvf - . --transform='s/^\.\///g' >root.tar
