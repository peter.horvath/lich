#!/usr/bin/bash
set -x
set -e
if [ "$1" = inst ]
then
  exec qemu-system-i386 -hda work/win7.img -cdrom dl/win7.iso -boot d -cpu qemu64 -m 1024 -vga cirrus -display vnc=0.0.0.0:1 -net nic,model=rtl8139 -net user
else
  # exec qemu-system-i386 -smp 4 -hda work/winxp.img -cdrom dl/winxp.iso -boot d -cpu qemu64 -m 512 -vga cirrus -display vnc=0.0.0.0:1 -net nic,model=rtl8139 -net user
  #  -netdev user,id=net0,ipv4=on,ipv6=off,hostname=xpap,domainname=lich,hostfwd=tcp::3389-:3389
  # -cdrom dl/winxp.iso \
  exec qemu-system-i386 -smp 4 -hda work/win7.img \
    -cdrom dl/win7.iso \
    -boot c -cpu qemu64 -m 3072 \
    -vga cirrus -display vnc=0.0.0.0:1 \
    -device virtio-net-pci,netdev=net0,mac="$(cat work/.win7mac)" \
    -netdev tap,id=net0,ifname=tap0,script=no,downscript=no \
    -monitor tcp:0.0.0.0:4444,server,nowait \
    -usb \
    -device usb-ehci,id=ehci \
    -device usb-host,hostbus=2,hostport=1.2
fi
