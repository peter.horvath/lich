Make targets:

inst-cd: boots installation iso, with disk image attached
preseed-cd: builds and starts preseeded install. After installation, the VM should exist.
sys-hd: boots disk image (installed by preseed-cd)
kill: kills all running qemu VMs
clean: deletes things, except what needs download or huge
distclean: deletes everything not source
fixperm: fixes permissions everywhere in the source
oldconfig: updates .config, asking if needed
menuconfig: ncurses ui to configure what do we want and how

---
Exit from guest (exit code 1):

dd if=/dev/zero of=/dev/port seek=1281 count=1 bs=1
